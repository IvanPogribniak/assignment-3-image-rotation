#include "../include/img_format.h"

struct pixel* get_pixel(uint64_t x, uint64_t y, struct image const* img){
    return img->data + x + y * img->width;
}

struct image create_image(uint64_t width, uint64_t height){
    struct pixel* data = malloc(width*height*sizeof(struct pixel));
    if (data != NULL){
        struct image img;
        img.width = width;
        img.height = height;
        img.data = data;
        return img;
    }
    return (struct image) {0};
}

int destroy_image(struct image* img){
    if (img != 0){
        img->width=0;
        img->height=0;
        free(img->data);
        img->data=NULL;
        return 1;
    }
    else{
        return 0;
    }
}

uint64_t get_size(struct image const* img){
    return img->height*img->width;
}
