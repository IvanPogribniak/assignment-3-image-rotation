#include "../include/bmp.h"
#include "../include/rotation.h"
#include <stdio.h>
int main( int argc, char** argv ) {
    if(argc!=3){
        fprintf(stderr, "Wrong amount of arguments, expected 2.\n");
        return -1;
    }
    FILE* in_file = fopen(argv[1], "rb");
    if (in_file == NULL){
        fprintf(stderr, "Error while opening input file, perhaps not enough permissions.\n");
        return -1;
    }
    struct image img = {0};
    enum read_status status = from_bmp(in_file, &img);
    if(status != READ_OK){
        fprintf(stderr, "Error while reading input file.\n");
        return -1;
    }
    if(fclose(in_file) == EOF){
        fprintf(stderr, "Error while closing input file.\n");
        return -1;
    }
    struct image rotated_image = rotate(&img);
    destroy_image(&img);
    FILE* out_file = fopen(argv[2], "wb");
    if (in_file == NULL){
        fprintf(stderr, "Error while opening output file, perhaps not enough permissions.\n");
        destroy_image(&rotated_image);
        return -1;
    }
    enum write_status write_status = to_bmp(out_file, &rotated_image);
    destroy_image(&rotated_image);
    if(write_status != WRITE_OK){
        fprintf(stderr, "Error while writing into output file.\n");
        destroy_image(&rotated_image);
        return -1;
    }
    if(fclose(out_file) == EOF){
        fprintf(stderr, "Error while closing output file.\n");
        destroy_image(&rotated_image);
        return -1;
    }
    fprintf(stdout, "Rotation completed.\n");
    return 0;
}
