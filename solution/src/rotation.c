#include "../include/rotation.h"
#include <stdio.h>
struct image rotate(struct image const* img){
    struct image rotated_image = create_image(img->height, img -> width);
    if(get_size(&rotated_image)==0){
        destroy_image(&rotated_image);
        return rotated_image;
    }
    for(uint32_t x = 0; x<img->height; x++){
        for(uint32_t y = 0; y<img->width; y++){
            *get_pixel(img->height-1-x, y, &rotated_image) = *get_pixel(y, x, img);
        }
    }
    return rotated_image;
}
