#include "../include/bmp.h"
static uint8_t padding_size(uint32_t width){
    return (PADDING_THRESHOLD - (width * sizeof(struct pixel))) % 4;
}

static struct bmp_header create_header(struct image const* img){
    uint32_t image_size = (sizeof(struct pixel)*img->width + padding_size(img->width))*img->height;
    return (struct bmp_header){
        .bfType = BMP_TYPE,
        .bfileSize = sizeof(struct bmp_header) + image_size,
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = INFO_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = RGB_BITCOUNT,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPelsPerMeter = PPX,
        .biYPelsPerMeter = PPX,
        .biClrUsed = NUM_COLORS,
        .biClrImportant = NUM_COLORS
    };
}

/*  deserializer   */


enum read_status from_bmp( FILE* in, struct image* img ){
    if (in == NULL || img == NULL){return READ_FILE_ERROR;}
    struct bmp_header header;
    if (fread(&header, HEADER_SIZE, 1, in) != 1){return READ_INVALID_HEADER;}
    *img = create_image((uint64_t)header.biWidth, header.biHeight);
    if(img->width*img->height==0){
        destroy_image(img);
        return READ_INVALID_HEADER;
    }
    for (uint32_t y=0; y<img->height; y++){
        if(fread(get_pixel(0, y, img), sizeof(struct pixel), img->width, in) != img->width || (fseek(in, padding_size(img->width), SEEK_CUR) != 0)){
            destroy_image(img);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

/*  serializer   */

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = create_header(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out) || fseek(out, HEADER_SIZE, SEEK_SET) || img -> data == NULL){
        return WRITE_ERROR;
    }
    char padding_array[4] = {0, 0, 0, 0};
    uint8_t padding = padding_size(img->width);
    for (uint32_t y=0; y<img->height; y++){
        if(fwrite(get_pixel(0, y, img), sizeof(struct pixel), img->width, out) != img->width != 0){
            return WRITE_ERROR;
        }
        if(fwrite(padding_array, sizeof(char), padding, out) != padding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
