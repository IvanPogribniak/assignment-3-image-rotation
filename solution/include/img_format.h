#ifndef IMG_FORMAT
#define IMG_FORMAT

#include <stdlib.h>

#include <stdint.h>
    struct pixel{
        uint8_t r, g, b;
    };

    struct image {
    uint64_t width, height;
    struct pixel* data;
    };

    struct pixel* get_pixel(uint64_t x, uint64_t y, struct image const* img);

    struct image create_image(uint64_t width, uint64_t height);

    int destroy_image(struct image* img);

    uint64_t get_size(struct image const* img);


#endif
