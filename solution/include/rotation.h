#ifndef ROT
#define ROT
#include "img_format.h"

struct image rotate(struct image const* img);

#endif
